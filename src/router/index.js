import Vue from 'vue'
import Router from 'vue-router'
import Home from '../views/home/Home'
import Cast from '../views/cast/Cast'
import Movie from '../views/movie/Movie'
import Book from '../views/book/Book'
import List from '../views/list/List'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/cast',
      name: 'Cast',
      component: Cast
    },    
    {
      path: '/movie',
      name: 'Movie',
      component: Movie
    },
    {
      path: '/book',
      name: 'Book',
      component: Book
    },
    {
      path: '/list',
      name: 'List',
      component: List
    }
  ]
})
